from django.contrib import admin

from django_stackers.apps.quiz.models import Question, Answer, Quiz

# admin.site.register(Quiz)
# admin.site.register(Question)
# admin.site.register(Answer)


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'created_at', 'updated_at')


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'quiz', 'question')


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id', 'answer', 'question', 'is_correct')
    ordering = ('question', )
