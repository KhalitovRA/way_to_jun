import django_filters
from .models import Quiz


class QuizFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Quiz
        fields = ['title']