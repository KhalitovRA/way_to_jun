from django.urls import path

from django_stackers.apps.quiz.views import QuizViewSet, QuestionViewSet, AnswerViewSet

quiz_list = QuizViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

quiz_detail = QuizViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

question_list = QuestionViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

question_detail = QuestionViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

answer_list = AnswerViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

answer_detail = AnswerViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})


urlpatterns = [
    path('quizzes/', quiz_list, name='quiz_list'),
    path('quizzes/<int:pk>/', quiz_detail, name='quiz-detail'),
    path('questions/', question_list, name='question-list'),
    path('questions/<int:pk>/', question_detail, name='question-detail'),
    path('answers/', answer_list, name='question-list'),
    path('answers/<int:pk>/', answer_detail, name='question-detail'),
]
