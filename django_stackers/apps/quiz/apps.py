from django.apps import AppConfig


class QuizConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_stackers.apps.quiz'
    verbose_name = 'Квизы'
