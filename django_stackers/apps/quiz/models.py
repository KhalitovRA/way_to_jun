from django.db import models


class Quiz(models.Model):

    title = models.CharField(
        max_length=240,
        verbose_name='Название квиза',
    )
    author = models.CharField(
        max_length=240,
        verbose_name='Автор квиза',
    )
    created_at = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата создания квиза',
    )
    updated_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата изменения квиза',
    )

    class Meta:
        verbose_name = 'Квиз'
        verbose_name_plural = 'Квизы'

    def __str__(self):
        return f'{self.title}'


class Question(models.Model):

    question = models.TextField(
        verbose_name='Вопрос'
    )
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE, null=True, related_name='questions')

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __str__(self):
        return f'{self.question}'


class Answer(models.Model):

    answer = models.CharField(
        verbose_name='Ответ на вопрос',
        max_length=200
    )
    question = models.ForeignKey('Question', on_delete=models.CASCADE, null=True, related_name='answers')
    is_correct = models.BooleanField(
        verbose_name='Верный ли ответ',
        default=False
    )

    def __str__(self):
        return f'{self.answer}'

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

